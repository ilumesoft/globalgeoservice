<?php
/**
* simpleGMapGeocoder | simpleGMapGeocoder is part of simpleGMapAPI
*                      Heiko Holtkamp, 2010
*
* Modified by Vasily Gibin | vasily@gibin.net
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*
* simpleGMapGeocoder
* is used for geocoding and is part of simpleGMapAPI
*
* @class        simpleGMapGeocoder
* @author       Heiko Holtkamp <heiko@rvs.uni-bielefeld.de>
* @modified_by	Vasily Gibin <vasily@gibin.net>
* @version      0.1.3
* @copyright    2010 HH
*/

class GlobalGeoService {

	/**
	* @function     geocode_address
	* @param        $address : string
	* @returns      -
	* @description  Gets GeoCoords by calling the Google Maps geoencoding API
	*/
	function geocode_address($address)
	{
		$coords = array();


		// call geoencoding api with param json for output
		$geoCodeURL = 'http://maps.google.com/maps/api/geocode/json?address='.
		urlencode($address).'&sensor=false';

		$getpage = '';
		$ok = false;

		if (ini_get('allow_url_fopen'))
		{
			if (file_exists($geoCodeURL))
			{
				$getpage = file_get_contents($geoCodeURL);
				$ok = true;
			}
		}

		if (!$ok)
		{
			if (function_exists('curl_init'))
			{
				$ch = curl_init();
				$timeout = 5; // set to zero for no timeout
				curl_setopt ($ch, CURLOPT_URL, $geoCodeURL);
				curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
				$getpage = curl_exec($ch);
				curl_close($ch);
			}
			else
			{
				return null;
			}
		}

		if (function_exists('mb_detect_encoding'))
		{
			$enc = mb_detect_encoding($getpage);

			if (!empty($enc))
				$getpage = mb_convert_encoding($getpage, 'UTF-8', $enc);
		}

		if ($getpage <>'')
		{
			$result = json_decode($getpage, true);

			$coords['status'] = $result['status'];

			if ( isset($result['results'][0]) )
			{
				$coords['lat'] = $result['results'][0]['geometry']['location']['lat'];
				$coords['lng'] = $result['results'][0]['geometry']['location']['lng'];
			}
		}

		return $coords;
	}

	/**
	* WORK IN PROGRESS...
	*
	* @function     lookup_address
	* @param        $lat : string
	* @param        $lng : string
	* @returns      -
	* @description  Gets Address for the given LatLng by calling the Google Maps geoencoding API
	*/
	function lookup_address($lat, $lng)
	{
		$address = array();

		// call geoencoding api with param json for output
		$geoCodeURL = 'http://maps.google.com/maps/api/geocode/json?address=$lat,$lng&sensor=false';

		$getpage = '';
		$ok = false;

		if (ini_get('allow_url_fopen'))
		{
			if (file_exists($geoCodeURL))
			{
				$getpage = file_get_contents($geoCodeURL);
				$ok = true;
			}
		}

		if (!$ok)
		{
			if (function_exists('curl_init'))
			{
				$ch = curl_init();
				$timeout = 5; // set to zero for no timeout
				curl_setopt ($ch, CURLOPT_URL, $geoCodeURL);
				curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
				$getpage = curl_exec($ch);
				curl_close($ch);
			}
			else
			{
				return null;
			}
		}

		if (function_exists('mb_detect_encoding'))
		{
			$enc = mb_detect_encoding($getpage);

			if (!empty($enc))
				$getpage = mb_convert_encoding($getpage, 'UTF-8', $enc);
		}

		if ($getpage <>'')
		{
			$result = json_decode($getpage, true);

			$address['status'] = $result['status'];
		}

		return $address;
	}

	/**
	* @function     geocode_address_osm
	* @param        $address : string
	* @returns      -
	* @description  Gets GeoCoords by calling the OpenStreetMap geoencoding API
	*/
	function geocode_address_osm($address)
	{
		$coords = array();

		// call OSM geoencoding api
		// limit to one result (limit=1) without address details (addressdetails=0)
		// output in JSON
		$geoCodeURL = 'http://nominatim.openstreetmap.org/search?format=json&limit=1&addressdetails=0&q='.
		urlencode($address);


		$getpage = '';
		$ok = false;

		if (ini_get('allow_url_fopen'))
		{
			if (file_exists($geoCodeURL))
			{
				$getpage = file_get_contents($geoCodeURL);
				$ok = true;
			}
		}

		if (!$ok)
		{
			if (function_exists('curl_init'))
			{
				$ch = curl_init();
				$timeout = 5; // set to zero for no timeout
				curl_setopt ($ch, CURLOPT_URL, $geoCodeURL);
				curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
				$getpage = curl_exec($ch);
				curl_close($ch);
			}
			else
			{
				return null;
			}
		}

		if (function_exists('mb_detect_encoding'))
		{
			$enc = mb_detect_encoding($getpage);

			if (!empty($enc))
				$getpage = mb_convert_encoding($getpage, 'UTF-8', $enc);
		}

		if ($getpage <>'')
		{
			$result = json_decode($getpage, true);

			if ( isset($result['results'][0]) )
			{
				$coords['lat'] = $result[0]['lat'];
				$coords['lng'] = $result[0]['lon'];
			}
		}

		return $coords;
	}
} // end of class
?>
