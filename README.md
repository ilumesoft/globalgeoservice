# Global GMap Service

This is a slighly modified version of the simpleGMapAPI 1.3.0 by Heiko Holtkamp [http://www.rvs.uni-bielefeld.de/~heiko/projects/simpleGMapAPI/ ](http://www.rvs.uni-bielefeld.de/~heiko/projects/simpleGMapAPI/).

It was modified for extension and a specific use case and published here to fit the requirements of the GPL.
